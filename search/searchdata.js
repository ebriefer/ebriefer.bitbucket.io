var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "acdefmnqrstu",
  2: "cehlmnpstv",
  3: "_abcdefgklmnoprstuvwxyz",
  4: "abcdefghijklmnprstuvxy",
  5: "cdehlmpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

