var annotated_dup =
[
    [ "controller", null, [
      [ "Controller", "classcontroller_1_1Controller.html", "classcontroller_1_1Controller" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "encoderDriver", null, [
      [ "Encoder", "classencoderDriver_1_1Encoder.html", "classencoderDriver_1_1Encoder" ]
    ] ],
    [ "FrontEnd", null, [
      [ "UI", "classFrontEnd_1_1UI.html", "classFrontEnd_1_1UI" ]
    ] ],
    [ "lab3", null, [
      [ "simonSays", "classlab3_1_1simonSays.html", "classlab3_1_1simonSays" ]
    ] ],
    [ "ME405_FP_ABFilter", null, [
      [ "ABFilter", "classME405__FP__ABFilter_1_1ABFilter.html", "classME405__FP__ABFilter_1_1ABFilter" ]
    ] ],
    [ "ME405_FP_DRV8847", null, [
      [ "DRV8847", "classME405__FP__DRV8847_1_1DRV8847.html", "classME405__FP__DRV8847_1_1DRV8847" ],
      [ "DRV8847_channel", "classME405__FP__DRV8847_1_1DRV8847__channel.html", "classME405__FP__DRV8847_1_1DRV8847__channel" ]
    ] ],
    [ "ME405_FP_Encoder", null, [
      [ "ME405_FP_Encoder", "classME405__FP__Encoder_1_1ME405__FP__Encoder.html", "classME405__FP__Encoder_1_1ME405__FP__Encoder" ]
    ] ],
    [ "ME405_FP_FSC", null, [
      [ "FSC", "classME405__FP__FSC_1_1FSC.html", "classME405__FP__FSC_1_1FSC" ]
    ] ],
    [ "ME405_FP_ResTP", null, [
      [ "ResTP", "classME405__FP__ResTP_1_1ResTP.html", "classME405__FP__ResTP_1_1ResTP" ]
    ] ],
    [ "me405_lab4", null, [
      [ "MCP9808", "classme405__lab4_1_1MCP9808.html", "classme405__lab4_1_1MCP9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "Motor", "classmotorDriver_1_1Motor.html", "classmotorDriver_1_1Motor" ]
    ] ],
    [ "nucleoSerial", null, [
      [ "nucleoSerial", "classnucleoSerial_1_1nucleoSerial.html", "classnucleoSerial_1_1nucleoSerial" ]
    ] ],
    [ "pid", null, [
      [ "ClosedLoop", "classpid_1_1ClosedLoop.html", "classpid_1_1ClosedLoop" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ]
];