/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CALPOLY MECHATRONICS", "index.html", [
    [ "MECHATRONICS DOCUMENTATION", "index.html", "index" ],
    [ "Simulation or Reality?", "HW0x04.html", [
      [ "Summary", "HW0x04.html#sec_hw0x04_summary", null ],
      [ "Numerical Parameters", "HW0x04.html#sec_hw0x04_parameters", null ],
      [ "Engineering Requirements", "HW0x04.html#sec_hw0x04_engineering_requirments", null ],
      [ "Matlab and Simulink Documentation", "HW0x04.html#sec_hw0x04_matlab_simlink", null ],
      [ "Simulation Plots", "HW0x04.html#sec_hw0x04_plots", null ]
    ] ],
    [ "Pole Placement", "HW0x05.html", [
      [ "Summary", "HW0x05.html#sec_hw0x05_summary", null ],
      [ "Engineering Requirements", "HW0x05.html#sec_hw0x05_engineering_requirements", null ],
      [ "Hand Calculations", "HW0x05.html#sec_hw0x05_Hand_Calculations", null ],
      [ "Matlab and Simulink", "HW0x05.html#sec_hw0x05_matlab_and_simulink", null ],
      [ "Results", "HW0x05.html#sec_hw0x05_Results", null ],
      [ "Discussion & Analysis", "HW0x05.html#sec_hw0x05_discussion", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW0x04.html",
"classlab3_1_1simonSays.html#a6e68f6391cae17fa1abf9fad705b4549",
"vendotron_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';